package epam.university.yunusov.model.domain;

public class Order {

  private static int unique = 1;
  private int id;
  private Pizza pizza;
  private String customerName;
  private String address;

  public Order(Pizza pizza, String customerName, String address) {
    this.pizza = pizza;
    this.customerName = customerName;
    this.address = address;
    this.id = unique;
    unique++;
  }

  public int getId() {
    return id;
  }

  public Pizza getPizza() {
    return pizza;
  }

  public String getCustomerName() {
    return customerName;
  }

  public String getAddress() {
    return address;
  }

  @Override
  public String toString() {
    return "id=" + id +
        ", pizza=" + pizza +
        ", customerName='" + customerName + '\'' +
        ", address='" + address;
  }

}
