package epam.university.yunusov.model.domain;

public enum Pizza {
  TEXAS, HAWAIIAN, FIVE_CHEESES, BBQ, BAVARIAN, CARBONARA, TUNA, MARGARITA
}
