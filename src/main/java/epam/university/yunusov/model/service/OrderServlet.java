package epam.university.yunusov.model.service;

import epam.university.yunusov.model.domain.Order;
import epam.university.yunusov.model.domain.Pizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {

  private static final Logger logger = LogManager.getLogger(OrderServlet.class);
  private static Map<Integer, Order> orders = new HashMap<>();

  @Override
  public void init() throws ServletException {
    logger.info("Servlet " + this.getServletName() + " has started");
    Order o1 = new Order(Pizza.BAVARIAN, "Michel", "kooperativna 25");
    Order o2 = new Order(Pizza.BBQ, "Igor", "franka 2");
    Order o3 = new Order(Pizza.TEXAS, "Eyvaz", "lychakivska 14");
    orders.put(o1.getId() ,o1);
    orders.put(o2.getId() ,o2);
    orders.put(o3.getId() ,o3);
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.info( this.getServletName() + " handling get request");
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><head><body>");
    out.println("<p> <a href='orders'>REFRESH</a> </p>");

    out.println("<h2>Orders List</h2>");
    for (Order order : orders.values()) {
      out.println("<p>" + order + "</p>");
    }
    out.println("\n\n<form action='orders' method='POST'>\n"
        + "  Pizza: <input type='text' name='pizza'>\n"
        + getAvailablePizzas()
        + "  Customer name: <input type='text' name='customer_name'>\n"
        + "  Address: <input type='text' name='address'>\n"
        + "  <button type='submit'>Add order</button>\n"
        + "</form>");
    out.println("<form>\n"
        + "  <p><b>Delete order</b></p>\n"
        + "  <p> Order id: <input type='text' name='order_id'>\n"
        + "    <input type='button' onclick='remove(this.form.order_id.value)' name='ok' value='Delete order'/>\n"
        + "  </p>\n"
        + "</form>");
    out.println("<script type='text/javascript'>\n"
        + "  function remove(id) { fetch('orders/' + id, {method: 'DELETE'}); }\n"
        + "</script>");
  }

  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.info(this.getServletName() + "post request");
    Pizza pizza = Pizza.valueOf(req.getParameter("pizza"));
    String customer_name = req.getParameter("customer_name");
    String address = req.getParameter("address");
    Order order = new Order(pizza, customer_name, address);
    logger.info("Add " + order);
    orders.put(order.getId(), order);
    doGet(req, resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException {
    logger.info(this.getServletName() + "delete request");
    String id = req.getRequestURI();
    logger.info("URI=" + id);
    id = id.replace("/pizza-servlets/orders/", "");
    logger.info("id=" + id);
    orders.remove(Integer.parseInt(id));
  }

  @Override
  public void destroy() {
    logger.info("Servlet " + this.getServletName() + " has stopped");
  }

  private String getAvailablePizzas() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("<h5>Available Pizzas</h5>");
    stringBuilder.append("<p>");
    for (Pizza pizza : Pizza.values()) {
      stringBuilder.append(pizza.toString() + ", ");
    }
    stringBuilder.append("</p>");
    return stringBuilder.toString();
  }
}
